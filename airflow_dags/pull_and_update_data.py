# Import airflow lbraries
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, ShortCircuitOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow import AirflowException

#Import other dependencies
from datetime import datetime, timedelta
import os
import requests
import json
import pymysql
import logging
import xml.etree.ElementTree as ET

# Import custom dependencies

import spark_API
import airflow_API

from pathlib import Path

CONFIGPATH = os.environ['PIPELINE_CONFIG']

root = ET.parse(CONFIGPATH).getroot()

sqldetails = {}
for child in root.findall('./SQLDetails/'):
    sqldetails[child.tag] = child.text

apiendpoints = {}
for child in root.findall('./APIEndPoints/'):
    apiendpoints[child.tag] = child.text

aws_details = {}
for child in root.findall('./AWSDetails/'):
    hdfs_settings[child.tag] = child.text

spark_settings = {}
for child in root.findall('./SparkSettings/'):
    spark_settings[child.tag] = child.text

AirflowSpecs = {}
for child in root.findall('./AirflowSpecs/'):
    AirflowSpecs[child.tag] = child.text

def fetch_and_store_data(**kwargs):

    bucket_name = kwargs['dag_run'].conf['aws_bucket']
    key_name = kwargs['dag_run'].conf['aws_bucket_key']
    file_path_details = kwargs['dag_run'].conf['file_path']
    till_date = kwargs['dag_run'].conf['end_date']
    currency_list = kwargs['dag_run'].conf['currencies']
    output_location = kwargs['dag_run'].conf['output_location']


    args = []
    args.append(aws_details)
    args.append(bucket_name)
    args.append(key_name)
    args.append(file_path_details)
    args.append(till_date)
    args.append(apiendpoints['APIURL'])
    args.append(apiendpoints['APIToken'])
    args.append(output_location)
    args.append(currency_list)



    kwargs['task_instance'].xcom_push(key='output_location',value=output_location)
    kwargs['task_instance'].xcom_push(key='output_file_path',value=file_path_details)


def dedup_and_update_data(**kwargs):

    #TODO: Add xcom pull values
    output_filepath = kwargs['task_instance'].xcom_pull(key='output_file_path',value=file_path_details)

    args = []
    args.append(output_filepath)
    args.append(spark_settings['master_url'])

    python_files_location = spark_settings['hdfs_pythonfiles']

    res = spark_API.pyspark_submit(python_files_location+"dqm_check_raw.py","dqmcodes",args,max_cores = 2)
    logging.info(res)
    clean_file = {}
    clean_file['submission_details'] = json.loads(res)
    status = spark_API.track_job(res)

    logging.info(status)



default_args = {
    'owner': 'dhananjay-ramani',
    'depends_on_past': False,
    'start_date': datetime(2016, 10, 15),
    'retries': 3,
    'retry_delay': timedelta(minutes=2),
    'dag_catchup': False
}

dag_base = DAG('currency_data_update', default_args=default_args,schedule_interval=None)

fetch_and_store_data =PythonOperator(
    task_id='fetch-data-to-s3',
    provide_context=True,
    python_callable=fetch_and_store_data,
    dag=dag_base
)


dedup_and_update_data=PythonOperator(
    task_id='push-incremental-data',
    provide_context=True,
    python_callable=dedup_and_update_data,
    dag=dag_base
)


# dag_started >> identify_jobspec
fetch_and_store_data >> dedup_and_update_data
