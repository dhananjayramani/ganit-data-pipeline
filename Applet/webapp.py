from flask import Flask, jsonify, render_template, request  # import flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import psycopg2

# Database connection setup
t_host = "database address"
t_port = "5432"
t_dbname = "database name"
t_user = "database user name"
t_pw = "database user password"
db_conn = psycopg2.connect(host=t_host, port=t_port, dbname=t_dbname, user=t_user, password=t_pw)
db_cursor = db_conn.cursor()



app = Flask(__name__)  # create an app instance


@app.route("/")  # at the end point /
def hello():  # call method hello
    return "Hello World!"




@app.route('/data', methods=['GET'])
def fetch_data():
    # Get historical data
    s = ""
    s += "SELECT * FROM fact_currency"

    db_cursor.execute(s)
    return {'results':
                [dict(zip([column[0] for column in db_cursor.description], row))
                 for row in db_cursor.fetchall()]}

    db_cursor.close()
    db_conn.close()

@app.route('/line_chart', methods=['GET'])
def chart():
    legend = 'Daily Data'
    x = fetch_data()
    labels = [a['Date'] for a in x]

    currencies = [a['Currency'] for a in x]
    cur = list(set(currencies))
    values = {}
    for i in cur:
        values[i] = []
        values[i].extend( [a if a['Currency']==i for a in x] )




# which returns "hello world"
if __name__ == "__main__":  # on running python app.py
    app.config['DEBUG'] = True
    app.run()