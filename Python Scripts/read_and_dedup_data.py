import requests
import json
import logging
import datetime
import pandas as pd
import numpy
import sqlalchemy
import boto3
import pickle
import pyspark



def read_and_dedup_data(filepath,master_url):
    sc = SparkContext(master_url, "Read And Dedup")

    sc.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", s3_params['AWS_ACCESS_KEY_ID'])
    sc.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey", s3_params['AWS_SECRET_ACCESS_KEY'])

    sqlContext = SparkContext.SQLContext()
    json_schema = spark.read.json(df.rdd.map(lambda row: row[columnName])).schema
    val
    df = sqlContext.jsonFile("s3://my_bucket_name/*/*/*", mySchema)

    # Remove duplicates
    df = df.drop_duplicates()

    # Return df
    return df

def create_fact_currency(db_params, table_name = "fact_currency"):
    host_name = db_params['host']
    db_name = db_params['database']
    username = db_params['user']
    password = db_params['pwd']
    url_string = db_params['ÚRLString']

    engine = create_engine(url_string)
    con = engine.connect()

    meta = MetaData()

    students = Table(
        'fact_currency', meta,
        Column('Date', Date, primary_key=True),
        Column('Currency', String, primary_key=True),
        Column('value', String),
    )
    meta.create_all(engine)

    con.close()

def create_dim_currency(db_params, table_name = "dim_currency"):
    host_name = db_params['host']
    db_name = db_params['database']
    username = db_params['user']
    password = db_params['pwd']
    url_string = db_params['ÚRLString']

    engine = create_engine(url_string)
    con = engine.connect()

    meta = MetaData()

    students = Table(
        'dim_currency', meta,
        Column('Currency', String, primary_key=True)
    )
    meta.create_all(engine)

    con.close()


def push_historical_data_to_postgres(df, db_params, table_name):
    host_name = db_params['host']
    db_name = db_params['database']
    username = db_params['user']
    password = db_params['pwd']

    url_string = db_params['ÚRLString']

    spark = SparkSession.builder.appName("Push data to postgres").getOrCreate()

    engine = create_engine(url_string)
    con = engine.connect()

    tables = engine.table_names()
    if (table_name not in tables):
        create_fact_currency(db_params, table_name)

    existing_data =.read
    .format("jdbc")
    .option("url", url_string)
    .option("dbtable", table_name)
    .option("user", username)
    .option("password", password)
    .load()

    incremental_data_df = df.join(existing_data, df("Date") === existing_data("Date") & & df("Currency") === existing_data("Currency"), "leftanti")

    incremental_data_df.write.format("jdbc")
    .option("url", "jdbc:postgresql:dbserver")
    .option("dbtable", table_name)
    .option("user", username)
    .option("password", password)
    .save()


def push_currency_dim_to_postgres(df, db_params, table_name):
    host_name = db_params['host']
    db_name = db_params['database']
    username = db_params['user']
    password = db_params['pwd']

    url_string = db_params['ÚRLString']

    spark = SparkSession.builder.appName("Push data to postgres").getOrCreate()

    engine = create_engine(url_string)
    con = engine.connect()

    tables = engine.table_names()
    if (table_name not in tables):
        create_dim_currency(db_params, table_name)

    existing_data = spark.read
    .format("jdbc")
    .option("url", url_string)
    .option("dbtable", table_name)
    .option("user", username)
    .option("password", password)
    .load()


cols = ['Currency']

incremental_data_df = df.join(existing_data, df("Currency") === existing_data("Currency"), "leftanti")

incremental_data_df.write.format("jdbc")
.option("url", "jdbc:postgresql:dbserver")
.option("dbtable", table_name)
.option("user", username)
.option("password", password)
.save()


def main():
    filepath = sys.argv[1]
    master_url = sys.argv[2]
    db_params = sys.argv[3]

    # Read and dedup the data
    df = read_and_dedup_data(filepath,master_url)
    push_historical_data_to_postgres(df,db_params, "fact_currency")
    push_currency_dim_to_postgres(df,db_params,"dim_currency")

if __name__ == "__main__":
    main()