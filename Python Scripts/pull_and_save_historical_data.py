# Import modules
import requests
import json
import logging
import json
import logging
import datetime
import pandas as pd
import numpy
import sqlalchemy
import boto3
import pickle
import pyspark
import sys
import boto3


def get_historical_data(url, api_token,till_date,currency_list):
    payload = {'start_at':'2018-01-01','end_at': str(till_date)}
    #print(payload)
    result = requests.get(url,params = payload)
    #print(result.url)
    #print(result.text)
    if type(result.status_code) == type(8):
        jobj = json.dumps(result.json())
        df = pd.read_json(hist)
        df = pd.concat([df, df['rates'].apply(pd.Series)], axis=1)
        df['Date'] = df.index
        df = df.reset_index()

        cols= []
        cols.append('Date')
        cols.extend(currency_list)

        df = df[cols]

        df = df.melt(id_vars = ['Date'], value_vars = currency_list, var_name = 'Currency')
        return (jobj,df)
    else:
        #print(result.text)
        logging.error("There was an error!Not Successful!")
        return (json.dumps({}), None)


def save_data_to_local(df, filepath):
    data  = df.to_json(filepath,orient = 'records')
    with open(filepath, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def save_data_to_s3(df, s3_params,bucket_name, key):
    s3 = boto3.client('s3')
    AWS_ACCESS_KEY_ID = s3_params['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = s3_params['AWS_SECRET_ACCESS_KEY']

    conn = boto3.connect_s3(AWS_ACCESS_KEY_ID,
                           AWS_SECRET_ACCESS_KEY)
    bucket = conn.create_bucket(bucket_name,
                                location=boto.s3.connection.Location.DEFAULT)

    data  = df.to_dict(orient = 'records')
    # Serialize the object
    serializedObject = pickle.dumps(data)

    # Write to Bucket named 'mytestbucket' and
    # Store the list using key myList001
    s3.put_object(Bucket=bucket_name, Key=key, Body=serializedObject)

def main():
    aws_details = sys.argv[1]
    bucket_name = sys.argv[3]
    key = sys.argv[4]
    filepath = sys.argv[5]
    till_date =  sys.argv[6]
    api_url = sys.argv[7]
    api_token = sys.argv[8]
    output_location = sys.argv[9]
    currency_list = sys.argv[10]

    # Read the data
    data = get_historical_data(api_url, api_token, till_date,currency_list)
    df = data[1]

    # Write the data
    if(output_location == 's3'):
        save_data_to_s3(df, aws_details, bucket_name, key)
    else:
        save_data_to_local(df,filepath)


if __name__ == "__main__":
    main()