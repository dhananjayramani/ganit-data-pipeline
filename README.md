# Ganit Data Pipeline

The following document explains the steps to run the application.

## Dependencies
The following dependencies are required:
  - Airflow
  - Spark
  - PostGreSQL database
  - Python packages:
    - json
    - requests
    - pyspark
    - flask
    - sqlalchemy
    - psycopg2
    - sys
    - airflow
    - datetime
    - os
    - logging
    - xml
    - pandas
    - pickle
    - boto3

## Setup:
For sandbox and testing, it is highly recommended to create a virtualenv with all dependencies installed

Step 1: Clone the git repo

Step 2: Install and configure necessary dependencies in virtual env

Step 3: Create a pipeline config file with values. This is an XML. A template has been provided in the repo

Step 4: Create an environment variable called PIPELINE_CONFIG. It should be assigned with the location to the pipeline config xml file

Step 5: Point your airflow DAGs location to the airflow_dags folder. 

Step 6: Start the flask app with the following arguments : t_host (hostname of PostGreSQL db server), t_port (port number of database endpoint), 
t_dbname (database name), t_user (database username), t_pw = (database password)
     Note: The arguments dont have to be named, however they have to be specified in exactly the same order. If any argument is not necessary pass   an empty object

Step 7: Trigger airflow DAG via the REST API and pass the following parameters in the DAG run conf:
         - aws_bucket: Name of bucket for data storage
         - aws_bucket_key: key name to store historical data file
         - file_path: path to local filesystem, incase not using s3
         - end_date: Date till which data has to be received
         - currencies = A python list of currency codes for which data is to be extrcted
         - output_location = 's3' incase of S3 storage. 'local' for local filesystem storage

Step 8: Trial run. Expected output:
        - Data downloaded and saved to output location
        - Spark job runs to dedup the data
        - Tables in postgres are updated. If table not exists, new ones will be created
        - Refreshing the webpage displays chart data




